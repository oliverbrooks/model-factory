# Model Factory

Generate and save objects and dependent objects using factories.

Works nicely with functional models such as [knex-model-wrapper](http://github.com/oliverbrooks/knex-model-wrapper). 

```js
var ModelFactory = require("model-factory");

var knex = require("knex"); 

var factory = ModelFactory(knex());

// Define a factory with name, table name and default values
factory.define(
    "Country",   // Factory name
    "countries", // Table name
    {
        name: "United Kingdom" // Default data
    } 
);

factory.define(
    "Address", 
    "addresses", 
    {
        city: "Brighton",
        country: factory.get("Country") // can reference another factory
    }
);

// Build an object (and children)
factory.build("Address", {city: "London"})
    .then(function (address) {
        console.log(address) // {city: "London", country: {name: "United Kingdom"}}
    });

// Build and save an object (and children)
//  Factory Name - vv         vv - data to use
factory.create("Address", {city: "London"})
    .then(function (address) {
        console.log(address) // {id: 1, city: "London", country: {id: 12, name: "United Kingdom"}}
    });
```



