"use_strict";

var lodash = require("lodash");
var Bluebird = require("bluebird");

/**
 * Generate a factory builder
 * @param {String} knex  configured of
 * @returns {Function} factory builder
 */
module.exports = function factoryBuilder (knex) {

  if (!knex) {
    throw new Error("a knex instance is required");
  }

  var factories = {};

  function Factory () {

  };

  return {
    /*
     * Set a factory up
     * @param {String} modelName        modelName object
     * @param {String} tableName        SQL table name
     * @param {Object} attrs  default data for the model, values can be promises
     */
    set: function setFactory (modelName, tableName, attrs) {

      var factoryConfig = {
        tableName: tableName,
        attrs: attrs
      };

      factories[modelName] = factoryConfig;

      return factoryConfig;
    },
    get: function getFactory (modelName) {
      return factories[modelName];
    },
    build: function build (modelName, attrs) {
      var factoryConfig = this.get(modelName);

      // assign defaults
      attrs = attrs || {};
      attrs = lodash.assign(factoryConfig.attrs, attrs);

      // If any functions are given as values then execute
      Object.keys(attrs).forEach( function (key) {
        if (typeof attrs[key] === "function") {
          attrs[key] = attrs[key]();
        }
      });

      // execute any promise values such as saving other factories
      return Bluebird.props(attrs).then(function (builtModel) {
        return builtModel;
      });
    },
    create: function save (modelName, attrs) {
      var factoryConfig = this.get(modelName);

      // assign defaults
      attrs = attrs || {};
      attrs = lodash.assign(factoryConfig.attrs, attrs);

      // If any functions are given as values then execute
      Object.keys(attrs).forEach( function (key) {
        if (typeof attrs[key] === "function") {
          attrs[key] = attrs[key](attrs[key]);
        }
      });

      // execute any promise values such as saving other factories
      return Bluebird.props(attrs).then(function (builtModel) {
        return knex(factoryConfig.tableName).insert(builtModel);
      });
    }
  };
};
